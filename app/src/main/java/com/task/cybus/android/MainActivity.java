package com.task.cybus.android;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.task.cybus.android.Model.Cybus;
import com.task.cybus.android.Rest.ApiClient;
import com.task.cybus.android.Rest.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    EditText emailEditText;
    EditText passwordEditText;
    Button Login;
    ApiInterface apiService;
    Context mContext;

    String username;
    String password;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        emailEditText= (EditText) findViewById(R.id.editText_email);
        passwordEditText= (EditText) findViewById(R.id.editText_password);
        Login= (Button) findViewById(R.id.button_Login);
        mContext=this;

        apiService= ApiClient.getClient().create(ApiInterface.class);


        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                username=emailEditText.getText().toString().trim();
                password=passwordEditText.getText().toString();

              Call<Cybus> call=apiService.getUserLogin(username,password);
                call.enqueue(new Callback<Cybus>() {
                    @Override
                    public void onResponse(Call<Cybus> call, Response<Cybus> response) {

                           //List<Cybus> cybususersList=response.body();


                        //for (int i=0;i<)

                            if (response.body().getEmail().equals(username)){
                                Intent intent=new Intent(MainActivity.this,Allbrands.class);
                                startActivity(intent);
                            }else{
                                Toast.makeText(mContext,"Incorrect UserName or Password",Toast.LENGTH_LONG).show();
                            }



                    }

                    @Override
                    public void onFailure(Call<Cybus> call, Throwable t) {

                    }
                });




            }
        });








    }
}
