package com.task.cybus.android.Rest;




import com.task.cybus.android.Model.Cybus;
import com.task.cybus.android.Model.CybusBrands;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by khurram on 11/04/2017.
 */

public interface ApiInterface {

    @FormUrlEncoded
    @POST("login")
    Call<Cybus> getUserLogin(@Field("user_name") String name,
                                @Field("password") String password);

    @GET("getAllBrands")
    Call<List<CybusBrands>> getBrandsDetails();



}
