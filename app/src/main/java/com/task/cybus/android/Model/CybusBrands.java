package com.task.cybus.android.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by khurram on 22/08/2017.
 */

public class CybusBrands {


/*
     "brand_name": "lahore food street ",
             "rating": "3",
             "brand_logo

            ": "20170220120235_(1).jpg"
*/


    @SerializedName("brand_name")
    private String brandName;

    @SerializedName("rating")
    private String rating;

    @SerializedName("brand_logo")
    private String BrandLogo;

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getBrandLogo() {
        return BrandLogo;
    }

    public void setBrandLogo(String brandLogo) {
        BrandLogo = brandLogo;
    }

}
