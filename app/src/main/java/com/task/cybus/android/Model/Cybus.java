package com.task.cybus.android.Model;


import com.google.gson.annotations.SerializedName;


import java.text.ParseException;
import java.util.ArrayList;

/**
 * Created by khurram on 23/04/2017.
 */

public class Cybus {


    @SerializedName("email")
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
