package com.task.cybus.android.Rest;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by khurram on 11/04/2017.
 */

public class ApiClient {

    public static final String BASE_URL="http://bataado.cybussolutions.com/Api_service/";
    private static Retrofit mRetrofit=null;

    public static Retrofit getClient(){
        if (mRetrofit==null){
            mRetrofit=new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return  mRetrofit;
    }

}
