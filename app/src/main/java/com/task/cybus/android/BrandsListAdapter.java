package com.task.cybus.android;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.task.cybus.android.Model.CybusBrands;

import java.util.ArrayList;

/**
 * Created by khurram on 09/01/2017.
 */

public class BrandsListAdapter extends RecyclerView.Adapter<BrandsListAdapter.BrandViewHolder> {

  

    private Context mContext;
    private ArrayList<CybusBrands> mCybusBrands;
    private OnItemClickListener mItemClickListener;


    public BrandsListAdapter(ArrayList<CybusBrands> movies, Context context){
        mCybusBrands =movies;
        mContext=context;
    }

    @Override
    public BrandViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.brandlistitem,parent,false);
        BrandViewHolder movieViewHolder=new BrandViewHolder(view);
        return  movieViewHolder;
    }

    @Override
    public void onBindViewHolder(BrandViewHolder holder, int position) {
        holder.bindMovie(mCybusBrands.get(position));
    }

    @Override
    public int getItemCount() {
        return mCybusBrands.size();
    }


    public class BrandViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        ImageView poster;
        TextView name;
     //   TextView rating;
        RatingBar ratingbar;



        public BrandViewHolder(View itemView) {
            super(itemView);
            name= (TextView) itemView.findViewById(R.id.textView_brandName);
          //  rating= (TextView) itemView.findViewById(R.id.textView_ratings);
            poster= (ImageView) itemView.findViewById(R.id.imageView_brandImage);
            ratingbar= (RatingBar) itemView.findViewById(R.id.ratingBar);

        }

        public void bindMovie(final CybusBrands brand){
            name.setText(brand.getBrandName());
         //   rating.setText(brand.getRating());
            if (brand.getBrandLogo()!=null || brand.getBrandLogo()!="") {
                Picasso.with(mContext).load("http://bataado.cybussolutions.com/admin/uploads/brand_logo/" + brand.getBrandLogo()).resize(150, 150).centerInside().into(poster);
            }

        if (brand.getRating()!=null) {
            ratingbar.setRating(Float.parseFloat(brand.getRating().toString()));
        }
        }


        @Override
        public void onClick(View v) {
            if (mItemClickListener!=null)
                mItemClickListener.onItemClick(v, getPosition());
        }



    }



    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }


}
